%global commit 4f69e5a6cd02627a891f2b15c2cf01bf4c87d23d
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Summary:       VBAN protocol open-source implementation
Name:          vban
Version:       2.1.0
Release:       2.20210223git%{shortcommit}%{?dist}
License:       GPLv3
Source:        https://github.com/quiniouben/%{name}/archive/%{commit}/%{name}-%{shortcommit}.tar.gz
URL:           https://github.com/quiniouben/%{name}
BuildRequires: gcc
BuildRequires: automake
BuildRequires: alsa-lib-devel
BuildRequires: jack-audio-connection-kit-devel
BuildRequires: pulseaudio-libs-devel

%description
vban project is an open-source implementation of VBAN protocol. VBAN is a simple 
audio over UDP protocol proposed by VB-Audio, see VBAN Audio webpage It is 
composed of several command-line tools allowing to stream audio coming from 
audio backend interfaces to VBAN stream (vban_emitter) or playout incoming VBAN 
stream to audio backend interfaces (vban_receptor), or send text over the vban 
protocol (vban_sendtext). Up to now, for audio tools, Alsa, PulseAudio and Jack 
audio backends have been implemented. A fifo (pipe) output is also existing, to 
allow chaining command-line tools, and a file output too (writing raw pcm data).

%prep
%autosetup -n %{name}-%{commit}

%build
./autogen.sh
%configure
%make_build

%install
%make_install

%files
%license COPYING
%doc README.md
%{_bindir}/vban_sendtext
%{_bindir}/vban_receptor
%{_bindir}/vban_emitter

%changelog
* Tue Feb 23 2021 spike <spike@fedoraproject.org> 2.1.0-2.20210223git4f69e5a
- Updated to latest git commit

* Sat Dec 28 2019 spike <spike@fedoraproject.org> 2.1.0-1.20191228gita2aaf01
- Initial package
